angular.module("app").service("agendaSrv", function() {
  var appuntamenti = [{
    id: 1,
    titolo: "Appuntamento con Angular",
    data: new Date(),
    descrizione: "Oggi ci sarà da divertirsi... Yahooooooo",
    luogo: "Domus Stella Maris",
    priorita: "#ffa500"
  }, {
    id: 2,
    titolo: "dentista",
    data: new Date(),
    descrizione: "oggi ci sarà da divertirsi... yahooooo",
    luogo: "domus stella maris",
    priorita: "#ff0000"
  }, {
    id: 3,
    titolo: "booohh",
    data: new Date(),
    descrizione: "oggi ci sarà da divertirsi... yahooooo",
    luogo: "domus stella maris",
    priorita: "#00ff00"
  }];

  var getAppuntamenti = function() {
    return appuntamenti;
  }

  var deleteAppuntamento = function(indice) {
    appuntamenti.splice(indice, 1);
  }

  var getAppuntamento = function(id) {
    return appuntamenti.find(function(el) {
      return el.id == id;
    });
  }

  var updateAppuntamento = function(app) {
    var nuovo = angular.copy(app);
    var indice = appuntamenti.findIndex(function(el) {
      return el.id == app.id;
    });
    appuntamenti.splice(indice, 1, nuovo);
  }

  var addAppuntamento = function(app) {
    var nuovo = angular.copy(app);
    var arrayId = [];
    appuntamenti.forEach(function(el) {
      arrayId.push(el.id);
    });
    var id = Math.max(...arrayId);
    console.log(id);
    nuovo.id = ++id;
    appuntamenti.push(nuovo);
  }

  return {
    getAppuntamenti: getAppuntamenti,
    deleteAppuntamento: deleteAppuntamento,
    getAppuntamento: getAppuntamento,
    updateAppuntamento: updateAppuntamento,
    addAppuntamento: addAppuntamento
  }
});
