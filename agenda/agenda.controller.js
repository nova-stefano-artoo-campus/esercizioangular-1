angular.module("app").controller("agendaCtrl", function($scope, appuntamenti, agendaSrv) {
  $scope.appuntamenti = appuntamenti;
  console.log($scope.appuntamenti);

  $scope.$watch("ricerca", function(ricerca) {
    console.log(ricerca);
  });

  $scope.$watch("colore", function(colore) {
    console.log(colore);
  });

  $scope.elimina = function(indice) {
    agendaSrv.deleteAppuntamento(indice);
  }
});
