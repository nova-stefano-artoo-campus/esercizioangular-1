angular.module("app").controller("nuovoCtrl", function($scope, agendaSrv, $state) {
  $scope.crea = function() {
    console.log($scope.appuntamento);
    agendaSrv.addAppuntamento($scope.appuntamento);
    $state.go("/");
  }

  $scope.back = function() {
    $state.go("/");
  }

  $scope.appuntamento = {};
  $scope.appuntamento.priorita = "#ff0000";
  $scope.appuntamento.data = new Date();
});
