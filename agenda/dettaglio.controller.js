angular.module("app").controller("dettaglioCtrl", function($scope, appuntamento, agendaSrv, $timeout, $state) {
  $scope.loading = true;
  $timeout(function() {
    $scope.appuntamento = appuntamento;
    console.log($scope.appuntamento);
    $scope.loading = false;
  }, 1000);//Ritardo di un secondo messo per simulare una chiamata al server

  $scope.modifica = function() {
    console.log($scope.appuntamento);
    agendaSrv.updateAppuntamento($scope.appuntamento);
    $state.go("/");
  }

  $scope.back = function() {
    $state.go("/");
  }
});
