angular.module("app").config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/");
  $urlRouterProvider.when("", "/");

  $stateProvider.state("/", {
    url: "/",
    controller: "agendaCtrl",
    templateUrl: "agenda/agenda.template.html",
    resolve: {
      appuntamenti: function(agendaSrv) {
        return agendaSrv.getAppuntamenti();
      }
    }
  }).state("dettaglio", {
    url: "/dettaglio/:id",
    controller: "dettaglioCtrl",
    templateUrl: "agenda/dettaglio.template.html",
    resolve: {
      appuntamento: function(agendaSrv, $stateParams) {
        return agendaSrv.getAppuntamento($stateParams.id);
      },
      onEnter: function(appuntamento) {
        console.log("sono entrato");
        console.log(appuntamento);
      },
      onExit: function() {
        console.log("sono uscito");
      }
    }
  }).state("crea", {
    url: "/crea",
    controller: "nuovoCtrl",
    templateUrl: "agenda/nuovo.template.html"
  });
});
